import React from 'react';
import Compoments from '../compoments';
import MobileLogo from '../assets/120x40.png';

const NavigationMobileDemo = () => {
    return (
        <>
            <Compoments.Navigation.Bar
                isMobile={true}
                mobileHeader={<><img src={MobileLogo} /> <p>O</p></>}
            >
                <Compoments.Navigation.Item>
                    Uživatelia
                </Compoments.Navigation.Item>

                <Compoments.Navigation.Item>
                    Licence
                </Compoments.Navigation.Item>

                <Compoments.Navigation.Item>
                    Export
                </Compoments.Navigation.Item>

                <Compoments.Navigation.Item>
                    Nápoveda
                </Compoments.Navigation.Item>

            </Compoments.Navigation.Bar>
            <Compoments.Content.Container
                header={<p>Zoznam Uzivatelov</p>}
            />
        </>
    );
}

export default NavigationMobileDemo;