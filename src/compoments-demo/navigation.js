import React from 'react';
import Compoments from '../compoments';

import MainLogo from '../assets/220x155.png';

const NavigationDemo = () => {
    return (
        <>
            <Compoments.Navigation.Bar
                footer={<p style={{ color: 'white' }}>Odhlasit sa</p>}
                header={<img src={MainLogo} />}
            >
                <Compoments.Navigation.Item>
                    Uživatelia
                </Compoments.Navigation.Item>

                <Compoments.Navigation.ItemGroup>
                    <Compoments.Navigation.Item>
                        Vytvoriť Uživatelia
                    </Compoments.Navigation.Item>

                    <Compoments.Navigation.Item>
                        Hromadný Import
                    </Compoments.Navigation.Item>
                </Compoments.Navigation.ItemGroup>

                <Compoments.Navigation.Item>
                    Licence
                </Compoments.Navigation.Item>

                <Compoments.Navigation.Item>
                    Export
                </Compoments.Navigation.Item>

                <Compoments.Navigation.Item>
                    Nápoveda
                </Compoments.Navigation.Item>

            </Compoments.Navigation.Bar>
            <Compoments.Content.Container
                header={<p>Zoznam Uzivatelov</p>}
                offSet={true}
            />
        </>
    );
}

export default NavigationDemo;