import React from 'react';
import PropTypes from 'prop-types';
import './container.scss';

const Container = ({ children, header, offSet }) => {
    return (
        <div className={`content-container ${offSet ? 'content-container-offset': ''}`}>
                <div className='content-container-header'>{header}</div>
                <div className='content-container-main'>
                    {children}
                </div>
        </div>
    );
}

Container.defaultProps = {
    offSet: false
}

Container.propTypes = {
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    offSet: PropTypes.bool
}

export default Container;