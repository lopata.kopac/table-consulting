import Navigation from './navigation';
import Content from './content';
import Table from './table';
import Card from './card';

export default { Navigation, Content, Table, Card };