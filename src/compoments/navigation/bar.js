import React from 'react';
import NavigacionContext from './nav-context';
import PropTypes from 'prop-types';
import './bar.scss';

const DestopNavigationBar = ({ data, header, footer }) => {
    return (
        <>
            <div className='navigation-destop'>
                <div className='navigacion-destop-header'>{header}</div>
                {data}
                <div className='navigacion-destop-footer'>{footer}</div>
            </div>
        </>
    );
}

const MobileNavigationBar = ({ data, mobileHeader }) => {
    return (
        <>
            <div className='navigation-mobile-header'>
                {mobileHeader}
            </div>
            <div className='navigation-mobile-tabs'>
                {data}
            </div>
        </>
    );
}

const Bar = ({ children, tabs, ...rest }) => {
    return (
        <NavigacionContext.Provider value={{ tabs: tabs }}>
            {tabs ? <MobileNavigationBar data={children} {...rest} /> : <DestopNavigationBar data={children} {...rest} />}
        </NavigacionContext.Provider>
    );
}

Bar.defaultProps = {
    tabs: false
}

Bar.propTypes = {
    tabs: PropTypes.bool,
    header: PropTypes.element,
    footer: PropTypes.element,
    mobileHeader: PropTypes.element,
}

export default Bar;